"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    length = len(x)
    arrayabs = np.zeros(length)
    for i in range(length):
        arrayabs[i] = np.sqrt(x[i]**2+y[i]**2+z[i]**2)
    
    return arrayabs

    

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    length = len(time)
    evalpoints = np.linspace(min(time),max(time),length)
    
    interpol = np.interp(evalpoints, time, data)
    return (interpol , np.linspace(min(time),max(time),length))

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    X = fft(x)
    N = len(X)
    n = np.arange(N)
    sr = 1/(time[1]-time[0])
    T = N/sr
    freq = n/T
    
    return (X, freq)