import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_handy.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
dictionary = dict({sensor_settings_dict["ID"]:{"acceleration_x":[],"acceleration_y":[],"acceleration_z":[],"timestamp":[]}})

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

tic = time.perf_counter()
t = 0
while t < measure_duration_in_s:
    toc = time.perf_counter()
    t = toc - tic
    print(t)
    print("%f %f %f"%accelerometer.acceleration)
    dictionary[sensor_settings_dict["ID"]]["acceleration_x"].append(accelerometer.acceleration[0])
    dictionary[sensor_settings_dict["ID"]]["acceleration_y"].append(accelerometer.acceleration[1])
    dictionary[sensor_settings_dict["ID"]]["acceleration_z"].append(accelerometer.acceleration[2])
    dictionary[sensor_settings_dict["ID"]]["timestamp"].append(t)
    time.sleep(0.001)
    
    
    
    
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
with h5py.File(path_h5_file, "w") as f:
    raw = f.create_group("RawData")
    grp = raw.create_group(sensor_settings_dict["ID"])
    dset1 = grp.create_dataset("acceleration_x", data = dictionary[sensor_settings_dict["ID"]]["acceleration_x"], dtype = "f")
    dset2 = grp.create_dataset("acceleration_y", data = dictionary[sensor_settings_dict["ID"]]["acceleration_y"], dtype = "f")
    dset3 = grp.create_dataset("acceleration_z", data = dictionary[sensor_settings_dict["ID"]]["acceleration_z"], dtype = "f")
    dset4 = grp.create_dataset("timestamp",data = dictionary[sensor_settings_dict["ID"]]["timestamp"], dtype = "f")
    dset1.attrs["unit"] = "m/s²"
    dset2.attrs["unit"] = "m/s²"
    dset3.attrs["unit"] = "m/s²"
    dset4.attrs["unit"] = "s"
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))




